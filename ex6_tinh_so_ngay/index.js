/**
 * input
 * nhập dữ liệu  tháng năm
 * process
 *nếu năm nhỏ hơn 1920
 thông báo năm lớn hơn 1920
 nếu tháng nhỏ hơn 0 hoặc tháng lớn hơn 13 dữ liệu ko hợp lệ
 xét năm nhuận 
 gọi biến số ngày, xét các tháng có 30 ngày, nêú tháng bằng các tháng đó thì số ngày =30
 tương tự với tháng 31 ngày
 xét tháng hai với số ngày là 29
 làm tương tự với năm ko nhuận, tháng 2 là 28 ngày

 * output
 *số ngày của tháng
 *
 */
document.getElementById('soNgay').onclick = function(){
    var thangHomNay = document.getElementById('nhapThang').value*1;
    var namHomNay = document.getElementById('nhapNam').value*1;
    var soNgay = 0;
    if (namHomNay <= 1920) {
        alert("năm cần lớn hơn 1920");
      }
    if (thangHomNay <= 0 ||thangHomNay >= 13
      ) {
        alert("dữ liệu không hợp lệ");
      }  
       if (
        (namHomNay % 4 == 0 && namHomNay % 100 != 0) ||
        namHomNay % 400 == 0){
            if(thangHomNay==1||thangHomNay==3||thangHomNay==5||thangHomNay==7||thangHomNay==8||thangHomNay==10||thangHomNay==12){
            soNgay = 31;}
            else if(thangHomNay==4||thangHomNay==6||thangHomNay==9||thangHomNay==11){
            soNgay = 30;}
            else if(thangHomNay==2){
                soNgay=29;
            }
            else{}
            document.getElementById('result').innerHTML= "tháng " + thangHomNay + " năm " + namHomNay + " có " + soNgay + " ngày ";
        }
        else {
                if(thangHomNay==1||thangHomNay==3||thangHomNay==5||thangHomNay==7||thangHomNay==8||thangHomNay==10||thangHomNay==12){
                soNgay = 31;}
                else if(thangHomNay==4||thangHomNay==6||thangHomNay==9||thangHomNay==11){
                soNgay = 30;}
                else if(thangHomNay==2){
                    soNgay=28;
                }
                else{}
                document.getElementById('result').innerHTML= "tháng " + thangHomNay + " năm " + namHomNay + " có " + soNgay + " ngày ";
            }
        
}