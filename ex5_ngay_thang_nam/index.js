/**
 * input
 * nhập dữ liệu ngày tháng năm
 * process
 *nếu năm nhỏ hơn 1920
 thông báo năm lớn hơn 1920
 nếu ngày và tháng nhỏ hơn 0 hoặc ngày lớn hơn 32 tháng lớn hơn 13 dữ liệu ko hợp lệ
 xét năm nhuận 
 xét tháng có 30 ngày mà nhập ngày lớn hơn 31 dữ liệ k hợp lệ
 *xét tháng 2 có hơn 30 ngày dữ liệu k hơp lê
 *ngày cuối năm thì ngày = 1 tháng = 1 năm +1
 ngày cuối tháng thì ngày = 1 tháng + 1
 bt thì ngày + 1
 tương tự với ngày hôm qua
 * output
 *ngày tháng năm của ngày hôm sau
 *
 */

document.getElementById("ngayMai").onclick = function () {
  var ngayHomNay = +document.getElementById("nhapNgay").value;
  var thangHomNay = +document.getElementById("nhapThang").value;
  var namHomNay = +document.getElementById("nhapNam").value;
  if (namHomNay <= 1920) {
    alert("năm cần lớn hơn 1920");
  }
  if (
    ngayHomNay <= 0 ||
    thangHomNay <= 0 ||
    ngayHomNay >= 32 ||
    thangHomNay >= 13
  ) {
    alert("dữ liệu không hợp lệ");
  }  if (
    (namHomNay % 4 == 0 && namHomNay % 100 != 0) ||
    namHomNay % 400 == 0
  ) {
    if (
      (thangHomNay == 2 && ngayHomNay >= 30) ||
      (thangHomNay == 4 && ngayHomNay >= 31) ||
      (thangHomNay == 9 && ngayHomNay >= 31) ||
      (thangHomNay == 6 && ngayHomNay >= 31) ||
      (thangHomNay == 11 && ngayHomNay >= 31)
    ) {
      alert("dữ liệu không hợp lệ");
    } else if (ngayHomNay == 31 && thangHomNay == 12) {
      ngayHomNay = 1;
      thangHomNay = 1;
      namHomNay++;
    } else if (
      (ngayHomNay == 29 && thangHomNay == 2) ||
      (ngayHomNay == 30 && (thangHomNay == 4 ||
      thangHomNay == 6 ||
      thangHomNay == 9 ||
      thangHomNay == 11 ))||
      (ngayHomNay == 31 && (thangHomNay == 1 ||
      thangHomNay == 3 ||
      thangHomNay == 5 ||
      thangHomNay == 7 ||
      thangHomNay == 8 ||
      thangHomNay == 12 ||
      thangHomNay == 10))
    ) {
      ngayHomNay = 1;
      thangHomNay++;
    } else {
      ngayHomNay++;
    }
    document.getElementById("result").innerHTML = ngayHomNay +"/"+thangHomNay+"/"+namHomNay;
  } else {
    if (
      (thangHomNay == 2 && ngayHomNay >= 2) ||
      (thangHomNay == 4 && ngayHomNay >= 31) ||
      (thangHomNay == 9 && ngayHomNay >= 31) ||
      (thangHomNay == 6 && ngayHomNay >= 31) ||
      (thangHomNay == 11 && ngayHomNay >= 31)
    ) {
      alert("dữ liệu không hợp lệ");
    } else if (ngayHomNay == 31 && thangHomNay == 12) {
      ngayHomNay = 1;
      thangHomNay = 1;
      namHomNay++;
    } else if (
        
            (ngayHomNay == 28 && thangHomNay == 2) ||
            (ngayHomNay == 30 && (thangHomNay == 4 ||
            thangHomNay == 6 ||
            thangHomNay == 9 ||
            thangHomNay == 11 ))||
            (ngayHomNay == 31 && (thangHomNay == 1 ||
            thangHomNay == 3 ||
            thangHomNay == 5 ||
            thangHomNay == 7 ||
            thangHomNay == 8 ||
            thangHomNay == 12 ||
            thangHomNay == 10))
          )   {
      ngayHomNay = 1;
      thangHomNay++;
    } else {
      ngayHomNay++;
      
    }
    document.getElementById("result").innerHTML = ngayHomNay +"/"+thangHomNay+"/"+namHomNay;
  
  }
  
};
document.getElementById("ngayHomQua").onclick = function () {
    var ngayHomNay = +document.getElementById("nhapNgay").value;
    var thangHomNay = +document.getElementById("nhapThang").value;
    var namHomNay = +document.getElementById("nhapNam").value;
    if (namHomNay <= 1920) {
      alert("năm cần lớn hơn 1920");
    }
    if (
      ngayHomNay <= 0 ||
      thangHomNay <= 0 ||
      ngayHomNay >= 32 ||
      thangHomNay >= 13
    ) {
      alert("dữ liệu không hợp lệ");
    }  if (
      (namHomNay % 4 == 0 && namHomNay % 100 != 0) ||
      namHomNay % 400 == 0
    ) {
      if (
        (thangHomNay == 2 && ngayHomNay >= 30) ||
        (thangHomNay == 4 && ngayHomNay >= 31) ||
        (thangHomNay == 9 && ngayHomNay >= 31) ||
        (thangHomNay == 6 && ngayHomNay >= 31) ||
        (thangHomNay == 11 && ngayHomNay >= 31)
      ) {
        alert("dữ liệu không hợp lệ");
      } else if (ngayHomNay == 1 && thangHomNay == 1) {
        ngayHomNay = 31;
        thangHomNay = 12;
        namHomNay--;
     
      }    else if(ngayHomNay==1 && thangHomNay==3){
        ngayHomNay=29;
        thangHomNay=2
    }
      else if (ngayHomNay==1 && (thangHomNay==5||thangHomNay==7||thangHomNay==10||thangHomNay==12)   
      ) {
        ngayHomNay = 30;
        thangHomNay--;
      } else if (ngayHomNay==1&&(thangHomNay==2||thangHomNay==4||thangHomNay==8||thangHomNay==9||thangHomNay==11)){
        ngayHomNay==31;
        thangHomNay--;
      }
      else{
        ngayHomNay--
      }
      document.getElementById("result").innerHTML = ngayHomNay +"/"+thangHomNay+"/"+namHomNay;
    } else {
        if (
            (thangHomNay == 2 && ngayHomNay >= 29) ||
            (thangHomNay == 4 && ngayHomNay >= 31) ||
            (thangHomNay == 9 && ngayHomNay >= 31) ||
            (thangHomNay == 6 && ngayHomNay >= 31) ||
            (thangHomNay == 11 && ngayHomNay >= 31)
          ) {
            alert("dữ liệu không hợp lệ");
          } else if (ngayHomNay == 1 && thangHomNay == 1) {
            ngayHomNay = 31;
            thangHomNay = 12;
            namHomNay--;
          }   else if(ngayHomNay==1 && thangHomNay==3){
            ngayHomNay=28;
            thangHomNay=2
        } 
          else if (ngayHomNay==1 && (thangHomNay==5||thangHomNay==7||thangHomNay==10||thangHomNay==12)   
          ) {
            ngayHomNay = 30;
            thangHomNay--;
          } else if (ngayHomNay==1&&(thangHomNay==2||thangHomNay==4||thangHomNay==8||thangHomNay==9||thangHomNay==11)){
            ngayHomNay==31;
            thangHomNay--;
          }
          else{
            ngayHomNay--
          }
      document.getElementById("result").innerHTML = ngayHomNay +"/"+thangHomNay+"/"+namHomNay;
    
    }
    
  };
